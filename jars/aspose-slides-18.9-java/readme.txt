Aspose.Slides for Java, PowerPoint Presentation management library
=============================================================================================================

'Aspose.Slides for Java' is a unique PowerPoint document processing API that enables Java applications to read, 
write and manipulate PowerPoint documents (PPT, PPTX, POS, PPS, POTX, PPSX) PDF, HTML and image file formats 
without using 'Microsoft PowerPoint'.

'Aspose.Slides for Java' is the first and most features-rich PowerPoint API that provides functionality to manage 
PowerPoint documents within your own applications. As with all Aspose Java APIs, 'Aspose.Slides for Java' 
is written in pure Java, incredibly priced and lightning fast. It is well suited for Desktop based and 
Enterprise JSP/JSF Web applications developed in Windows, Unix/Linux and Mac platforms.

For more info see 'http://www.aspose.com/java/powerpoint-component.aspx'

------------------------------------------------------------------------------------------------------------

This distribution contains the following directories and files:

- 'docs'                                           : API Documentation in html format

- 'lib'                                            : 
-- 'aspose.slides-x.x.x.jar'                       : 'Aspose.Slides for Java' for JDK 1.6 - 1.8

- 'licenses'                                       : 
-- 'Aspose End User License Agreement.html'        : link to Aspose EULA 
-- 'thirdpartylicenses-Aspose.Slides for Java.pdf' : Notices and Licenses required for redistribution of 'Aspose.Slides for Java'

------------------------------------------------------------------------------------------------------------

For start please check:
 - 'Programmer`s Guide' in 'http://www.aspose.com/docs/display/slidesjava/Home'
 - 'API Docs' in 'doc' directory -or- in 'http://www.aspose.com/api/java/slides'

------------------------------------------------------------------------------------------------------------

Required Software and Libraries:
- Java runtime


=============================================================================================================
http://www.aspose.com/
Copyright (c) 2001-2018 Aspose Pty Ltd. All Rights Reserved.
